from flask import Flask, jsonify,request
import requests
import json
import hashlib
import pymongo
import jwt
from collections import Counter
app = Flask(__name__)

#intializing mongodb database
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#Creating a Database
mydb = myclient["crud_database"]
#Creating A collection
Mongodb_Data = mydb["crud_table"]
@app.route('/register',methods=["POST"])
def register():
   try:
      #Accepting json request here
      Getting_Postman_Data=request.json
      #check wheather the email id allready exist or not we use count method for that if it is 0 then we insert the data
      check=Mongodb_Data.find({"_id":Getting_Postman_Data['email_id']})
      print(check)
      if check.count()>=1:
         return jsonify({'response':False},"User allready exist" )
      else:
         #encrypting the password
         hashed = hashlib.sha1(Getting_Postman_Data["password"].encode())
         hashed=hashed.hexdigest()

         #insert data if the email is not prresent
         Insert_Data = { "_id": Getting_Postman_Data['email_id'], "name": Getting_Postman_Data["name"],"age":Getting_Postman_Data["age"],"company_name":Getting_Postman_Data["company_name"],"password":hashed,"mobile":Getting_Postman_Data["mobile"]}
         x = Mongodb_Data.insert_one(Insert_Data)
         print(hashed.decode())
         #after insertion of data we will send a success response to postman
         return jsonify({"response": True}, 200)
   #Here the exception is handled ie every other error will be handled here
   except Exception as e:
      return jsonify({"response":False},str(e))      
@app.route('/login',methods=["POST"])
def login():
   pass
   
if __name__ == '__main__':
   app.run(debug=True, port=5000)