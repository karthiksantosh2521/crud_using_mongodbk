from flask import Flask, jsonify,request
import requests
import json
import hashlib
import pymongo
import jwt
from collections import Counter
app = Flask(__name__)
secret="9\x06\xea{\x83\xb0\x85`\xdfZ\xd9i"
#intializing mongodb database
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#Creating a Database
mydb = myclient["crud_database"]
#Creating A collection
Mongodb_Data = mydb["crud_table"]
@app.route('/register',methods=["POST"])
def register():
   try:
      #Accepting json request here
      Getting_Postman_Data=request.json
      #check wheather the email id allready exist or not we use count method for that if it is 0 then we insert the data
      check=Mongodb_Data.find({"_id":Getting_Postman_Data['email_id']})
      print(check)
      if check.count()>=1:
         return jsonify({'response':False},"User allready exist" )
      else:
         #encrypting the password
         hashed = hashlib.sha1(Getting_Postman_Data["password"].encode())
         hashed=hashed.hexdigest()
         #insert data if the email is not prresent
         Insert_Data = { "_id": Getting_Postman_Data['email_id'], "name": Getting_Postman_Data["name"],"age":Getting_Postman_Data["age"],"company_name":Getting_Postman_Data["company_name"],"password":hashed,"mobile":Getting_Postman_Data["mobile"]}
         x = Mongodb_Data.insert_one(Insert_Data)
         
         #after insertion of data we will send a success response to postman
         return jsonify({"response": True}, 200)
   #Here the exception is handled ie every other error will be handled here
   except Exception as e:
      return jsonify({"response":False},str(e))      
@app.route('/login',methods=["POST"])
def login():
   #Accepting json request here
   Getting_Postman_Data=request.json
   #checking wheather E-mail exist
   Emaiil_exist=Mongodb_Data.find_one({'_id': Getting_Postman_Data['email_id']}) 
   #if email exist then check for password
   if Emaiil_exist:
      if(Emaiil_exist['password']==Getting_Postman_Data['password']):
         #Here we created a jwt token and sent returned the response
         encode_jwt=jwt.encode(Emaiil_exist,secret,algorithm="HS256")
         return jsonify({"response":True},{"status":f'Acess Granted'}, {"Token": str(encode_jwt)})
      else:
         #Will return a Error response if the password is not entered right
         return jsonify({"response":False},"Wrong password")
   else:
      #Will return a Error response if the E-mail is not entered right
      return jsonify({"response":False},"User does not exist please register yourself")
if __name__ == '__main__':
   app.run(debug=True, port=5000)